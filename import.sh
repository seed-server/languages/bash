#!/bin/bash

for directory in $(find $SEED_DIR -mindepth 1 -type d -name "__bash")
do
    for script in $(find $directory -mindepth 1 -type f)
    do
        source "$script"
    done
done
