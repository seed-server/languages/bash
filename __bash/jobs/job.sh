#==============================================================

function JOBS_SETUP() {

#==============================================================
    if [ -d "./.jobs" ]
    then
       rm -R "./.jobs"
    fi
   mkdir -p "./.jobs"
}
#==============================================================

function JOB_EXEC() {

#==============================================================
   local pid=$!
    echo $pid #
    #$$
   DEBUG true
   #echo "$@" > ./.jobs/$pid.cmd
   #echo "source $MAIN_PATH/setup.sh" >>  ./.jobs/$pid.cmd
   #echo "$@ > ./.jobs/$pid.out 2> ./.jobs/$pid.err" >>  ./.jobs/$pid.cmd
   #bash ./.jobs/$pid.cmd
   #TITLE "$pid $@"
   #cat ./.jobs/$pid.out
   #cat ./.jobs/$pid.err
   $@ 
}
#==============================================================

function JOB_EXEC_FILEOUT() {

#==============================================================
   local pid=$!
    echo $pid #
    #$$
   DEBUG true
   #echo "$@" > ./.jobs/$pid.cmd
   #echo "source $MAIN_PATH/setup.sh" >>  ./.jobs/$pid.cmd
   #echo "$@ > ./.jobs/$pid.out 2> ./.jobs/$pid.err" >>  ./.jobs/$pid.cmd
   #bash ./.jobs/$pid.cmd
   #TITLE "$pid $@"
   #cat ./.jobs/$pid.out
   #cat ./.jobs/$pid.err
   echo "$@" >  ./.jobs/$pid.cmd
   $@ > ./.jobs/$pid.out 2> ./.jobs/$pid.err
}
#==============================================================

function JOB() {

#==============================================================

   MESSAGE "JOB $@"
   JOB_EXEC "$@" &
}
#==============================================================

function JOBS_LS() {

#==============================================================
# list currents jobs

    if [ -z "$1" ]
    then
        local pid="$$"

    else
        local pid="$1"
        echo "$pid"
    fi
    

    if children="$(pgrep -P "$pid")"
    then
        for child in $children
        do
            JOBS_LS "$child"
        done
    fi

}
#==============================================================

function JOBS_WAIT() {

#==============================================================


    if [ -z "$1" ]
    then
        local pid="$$"

    else
        local pid="$1"
    fi
    

    if children="$(pgrep -P "$pid")"
    then
        for child in $children
        do
            wait "$child"

        done
    fi


}
#==============================================================

function JOBS_KILL () {

#==============================================================

    if [ -z "$1" ]
    then
        local pid="$$"

    else
        local pid="$1"
    fi
    
    local and_self="${2:-false}"
    
    if children="$(pgrep -P "$pid")"
    then
        for child in $children
        do
            MESSAGE "child process $child"
            JOBS_KILL "$child" true
        done
    fi
    
    if [[ "$and_self" == true ]]
    then

        ps $pid  > /dev/null
        local ps_exist=$?


        if [ $ps_exist == 0 ]
        then
            MESSAGE "kill $pid"
            kill "$pid" &> /dev/null
        fi
    fi
}
#==============================================================
