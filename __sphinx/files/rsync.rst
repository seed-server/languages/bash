
rsync : copie de répertoires
=================================================

faire une copie d'un répertoire à l'identique.
attention effacera les fichiers qui ne correspondent pas à la source dans la destination ::

   RSYNC_MIRROR [path_to_source] [path_to_destination]
   
   
ajouter des fichiers du répertoire source vers le répertoire destination sans toucher aux fichiers du répertoire destination ::

   RSYNC_ADD [path_to_source] [path_to_destination]